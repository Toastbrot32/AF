import cv2 as cv
import numpy as np
from terminaltables import AsciiTable


def circ_in_img(StartIm, Hough1, col, debug):
    I_HSV = cv.cvtColor(StartIm, cv.COLOR_BGR2HSV)#HSV konvertierung

    lowerthresh = np.array([0,125,125])
    upperthresh = np.array([20,255,255])

    #mask1 = cv.inRange(I_HSV, np.array([0,175,175]), np.array([0+redt,255,255]))
    #mask2 = cv.inRange(I_HSV, np.array([180-redt,175,175]), np.array([180,255,255]))

    c=col[0]
    w=col[1]

    mask1 = cv.inRange(I_HSV, np.array([c-w,125,125]), np.array([c+w,255,255]))
    mask2 = cv.inRange(I_HSV, np.array([180+c-w,125,125]), np.array([180+c+w,255,255]))

    mask=np.uint8(np.logical_or(mask1, mask2))

    res = cv.bitwise_and(StartIm,StartIm, mask= mask)

    if debug == 1:
        cv.imwrite('start.jpg',StartIm)
        #cv.waitKey(0)
        cv.imwrite('res.jpg',res)
        #cv.waitKey(0)

    img=np.uint8(255*(1-mask))
    img = cv.blur(img,(10,10))
    cimg = res
    circles = cv.HoughCircles(img,cv.HOUGH_GRADIENT,1,200,param1=50,param2=Hough1,minRadius=20,maxRadius=0)
    if circles is None:
        return 0
    else:
        if debug == 1:
            #print('Rot gefunden')
            #return 1
            circles = np.uint16(np.around(circles))
            for i in circles[0,:]:
                # draw the outer circle
                cv.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
                # draw the center of the circle
                cv.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
            cv.imwrite('circ.jpg',cimg)
            #cv.waitKey(1)
        return 1

def pstatus(data):
    table = AsciiTable(data)
    print(table.table)

def remstat(data):
    l=len(data)
    print ("\033[F\033[F")
    print ("\033[F\033[F")
    print ("\033[F\033[F")
    for i in range(0,l):
        print ("\033[F\033[F")


# for j in range(0,40):
    
#     ret, StartIm = cam.read()
#     #StartIm = cv.imread('J.png',1)

#     debug = 1
#     data=[]
#     data.append(['STATUS','OK'])

#     StatusR=circ_in_img(StartIm,40,[0,10],debug)
#     data.append(['ROT',StatusR])

#     #StatusO=circ_in_img(StartIm,40,[20,5],debug)
#     #data.append(['ORANGE',StatusO])

#     #StatusG=circ_in_img(StartIm,20,[55,20],debug)
#     #data.append(['GRUEN',StatusG])

#     pstatus(data)

#     #cv.imshow('detected circles',cimg)
#     #cv.waitKey(1)
#     #cv.destroyAllWindows()
#     time.sleep(0.5)
#     #print(chr(27) + "[2J")
#     print ("\033[F\033[F")
#     remstat(data)

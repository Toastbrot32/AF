import cv2
import numpy as np
import time

def bei_kreuzung(frame):
    dist = 1 
    distl = 1
    detect = False
    cnt = 1 
    frame = cv2.GaussianBlur(frame, (5, 5), 0) 
    frame=frame[250:430,180:460] 
    thet = 30 * np.pi/180 
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) 
    edges = cv2.Canny(gray, 75, 150) 
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 25, maxLineGap=10)
    if lines is not None:
        for line in lines:
            x1, y1, x2, y2 = line[0]
            xdiff = max((x2-x1),0.1)
            ang = abs(np.arctan((y2-y1)/(xdiff)))
            if ang < thet:
             cnt = cnt + 1
             distl = (y2+y1)/2 
             #cv2.line(frame, (x1, y1), (x2, y2), (0, 255, 0), 5)            
            dist= dist+ (distl-dist)/cnt
    if dist > 50 and dist < 90: #Kreuzung erst ab einem gewissen Abstand fuer Steuerinputs nutzen
        cv2.line(frame, (1, int(dist)), (250, int(dist)), (255, 0, 0), 5) 
        detect= True #True/false Variable. Kreuzung/keine Kreuzung
    if dist < 50 or dist > 90:
        detect= False
   
    return detect

import numpy as np
import cv2 as cv
import time
import math


def line_to_points(rho, theta):
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 640*(-b))
    y1 = int(y0 + 640*(a))
    x2 = int(x0 - 640*(-b))
    y2 = int(y0 - 640*(a))
    return x1, y1, x2, y2

def drawline(rho,theta,frame,c):
    x1, y1, x2, y2 = line_to_points(rho, theta)
    cv.line(frame,(x1,y1),(x2,y2),c,2)

def frame_to_lines(frame,tr,w,c1,c2):
    l_img=street(frame)
    debug=1
    (height,width,d)=l_img.shape
    l_img=cv.blur(l_img,(5,5))
    gray = cv.cvtColor(l_img, cv.COLOR_BGR2GRAY)
    edges = cv.Canny(gray, c1, c2)
    #ret, edges = cv.threshold(gray,200,255,cv.THRESH_BINARY)
    lines1 = cv.HoughLines(edges,1,np.pi/180,tr,0,0,0,0,w)
    lines2 = cv.HoughLines(edges,1,np.pi/180,tr,0,0,0,np.pi-w,np.pi)
    #print('-----')
    if lines1 is not None and lines2 is not None:
        lines=np.concatenate((lines1,lines2),axis=0)
        
        avgl1_rho=np.sum(lines1[:,0,0])*(1/(lines1[:,0,0].size))
        avgl1_theta=np.sum(lines1[:,0,1])*(1/(lines1[:,0,1].size))
        
        avgl2_rho=np.sum(lines2[:,0,0])*(1/(lines2[:,0,0].size))
        avgl2_theta=np.sum(lines2[:,0,1])*(1/(lines2[:,0,1].size))
        
        avg_theta=((np.pi/2-avgl1_theta) + (avgl2_theta-np.pi/2) )/2
        avg_rho=(avgl1_rho-avgl2_rho)/2

        #print(str(avg_theta) + ' ' + str(avgl1_theta) + ' ' + str(avgl2_theta ))
        #print(str(avg_rho) + ' ' + str(avgl1_rho) + ' ' + str(avgl2_rho ))
        
        avg_theta=np.pi+((avgl1_theta) + (avgl2_theta-np.pi) )/2
        avg_rho=(avgl1_rho-avgl2_rho)/2
        
        if avg_theta >np.pi :
            avg_theta=avg_theta-np.pi
        else :
            avg_rho=-(avgl1_rho-avgl2_rho)/2
            
        if not avgl2_theta==0:
            xl2=(-height+avgl2_rho/math.sin(avgl2_theta))*(math.sin(avgl2_theta)/math.cos(avgl2_theta))
            yl2=avgl2_rho/math.cos(avgl2_theta)
            ang=(avg_theta-np.pi/2)
        else:
            xl2=width/2
            yl2=0
            angl1=(avg_theta-np.pi/2)
        
        if not avgl1_theta==0:
            xl1=(-height+avgl1_rho/math.sin(avgl1_theta))*(math.sin(avgl1_theta)/math.cos(avgl1_theta))
            yl1=avgl1_rho/math.cos(avgl1_theta)
            angl1=(avg_theta-np.pi/2)
        else:
            xl1=width/2
            yl1=0
            angl1=(avg_theta-np.pi/2)
        avg_x=(xl1+xl2)/2
        avg_y=(yl1+yl2)/2
        
        if avg_theta >np.pi/2 :
            avg_rho=-avg_x*math.cos(avg_theta-np.pi)
        else :
            avg_rho=avg_x*math.cos(avg_theta)
            
        #cv.line(frame,(0,0),(int(avg_x),height),(0,0,255),2)
        
    if lines2 is None and lines1 is not None:
        lines=lines1
        
        avgl1_rho=np.sum(lines1[:,0,0])*(1/(lines1[:,0,0].size))
        avgl1_theta=np.sum(lines1[:,0,1])*(1/(lines1[:,0,1].size))

        avg_theta=avgl1_theta
        avg_rho=avgl1_rho

        if not avgl1_theta==0:
            avg_x=(-height+avgl1_rho/math.sin(avgl1_theta))*(math.sin(avgl1_theta)/math.cos(avgl1_theta))
            avg_y=avgl1_rho/math.cos(avgl1_theta)
            angl1=(avg_theta-np.pi/2)
        else:
            avg_x=width/2
            avg_y=0
            angl1=(avg_theta-np.pi/2)
        
        avgl2_rho=0
        avgl2_theta=0

        #drawline(avgl1_rho,avgl1_theta,frame,(0,255,0))
        
    if lines1 is None and lines2 is not None:
        lines=lines2
        
        avgl1_rho=0
        avgl1_theta=0
        
        avgl2_rho=np.sum(lines2[:,0,0])*(1/(lines2[:,0,0].size))
        avgl2_theta=np.sum(lines2[:,0,1])*(1/(lines2[:,0,1].size))

        avg_theta=avgl2_theta
        avg_rho=avgl2_rho

        if not avgl2_theta==0:
            avg_x=(-height+avgl2_rho/math.sin(avgl2_theta))*(math.sin(avgl2_theta)/math.cos(avgl2_theta))
            avg_y=avgl2_rho/math.cos(avgl2_theta)
            angl1=(avg_theta-np.pi/2)
        else:
            avg_x=width/2
            avg_y=0
            angl1=(avg_theta-np.pi/2)

        #drawline(avgl2_rho,avgl2_theta,frame,(0,255,0))
        
    if lines1 is None and lines2 is None:
        lines=None
        
        avgl1_rho=0
        avgl1_theta=0
        
        avgl2_rho=0
        avgl2_theta=0

        avg_rho=0
        avg_theta=0

        avg_x=width/2
        avg_y=0

    if debug == 1:
        if lines is not None:
            for rho,theta in lines[:,0,:]:
                drawline(rho,theta,frame,(255,0,0))
                
        if lines1 is not None and lines2 is not None:   
            drawline(avgl1_rho,avgl1_theta,frame,(0,255,0))
            drawline(avgl2_rho,avgl2_theta,frame,(0,255,0))
            
        if lines2 is None and lines1 is not None:
            drawline(avgl1_rho,avgl1_theta,frame,(0,255,0))
            #print(line_to_points(avgl1_rho,avgl1_theta))
            
        if lines1 is None and lines2 is not None:
            drawline(avgl2_rho,avgl2_theta,frame,(0,255,0))
            #print(line_to_points(avgl2_rho,avgl2_theta))

    (height,width,d)=frame.shape

    #if not avg_theta==0:
    #x=(-height+avgl1_rho/math.sin(avgl1_theta))*(math.sin(avgl1_theta)/math.cos(avgl1_theta))
    #y=avgl1_rho/math.cos(avgl1_theta)
    #ang=(avgl1_theta-np.pi/2)

    cv.line(frame,(int(avg_x),height),(int(avg_y),0),(0,0,255),2)

    a=avg_theta
    if avg_theta >np.pi/2:
        a = avg_theta-np.pi

    return avg_x, avg_theta, a

def street(img):
    I = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    ret, thresh1 = cv.threshold(I,130,255,cv.THRESH_BINARY)
    thresh = 255*np.uint8(np.logical_not(thresh1))
    ret, l_img=cv.connectedComponents(thresh,cv.CV_32S)#labeling

    (x,y)=I.shape

    y1=x-100
    x1=int(y/2-125)
    y2=x
    x2=int(y/2+125)

    a=l_img[y1:y2,x1:x2]
    cv.rectangle(img,(x1,y1),(x2-1,y2-1),(0,255,0))
    labels=np.unique(a)#Anzal der verschiedenen labels

    if labels[0]==0:
            np.delete(labels, 0)

    if labels.size==0:
        cv.rectangle(img,(x1,x2),(y1,y2),(0,255,0))
        return img
    else:

        numlabel=np.zeros(np.amax(labels))#Allocation

        for i in labels[1:labels.size]:
            numlabel[i-1]=np.count_nonzero(l_img==i)#Anzahl der Pixel pro label

        if not numlabel.size ==0:
            maxlabel=np.argmax(numlabel)+1#Label mit den meisten Pixeln
        else:
            return img

        I = 255*(l_img == maxlabel)#Auswahl der groessten Region
        I=np.uint8(I)# Konvertierung

        I=cv.cvtColor(I, cv.COLOR_GRAY2RGB)#Konvertierung zu RGB
        img[(l_img == maxlabel)]=(80,127,255)

    return I


# video = cv.VideoCapture("Halle3.avi")

# for j in range(0,300):
#     ret, frame = video.read()
#     frame=frame[150:400:]
#     h_sep=50

#     s=street(frame)

#     #frame=s
#     #s1=s[0:h_sep,:]
#     #s2=s[h_sep:300,:]

#     frame1=frame[0:h_sep,:]
#     frame2=frame[h_sep:300,:]

#     #p1, a1 , a= frame_to_lines(frame2,45,0.75,50,200)
#     a= frame_to_lines(frame2,45,0.75,50,200)
#     #p2, a2, b = frame_to_lines(s1,frame1,25,1.5,100,200)

#     #print(str(p1) + ' ' + str(a1))
#     #print(str(p2) + ' ' + str(a2))

#     #cv.line(frame,(0,h_sep),(640,h_sep),(0,255,0),2)
#     cv.imshow('asd',frame)
#     cv.waitKey(0)
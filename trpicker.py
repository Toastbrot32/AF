import numpy as np
import cv2 as cv
import math
import time
#from terminaltables import AsciiTable

for j in range(0,20):
    Im =cv.imread('I.png',1)
    I_HSV = cv.cvtColor(Im, cv.COLOR_BGR2HSV)#HSV konvertierung

    lowerthresh = np.array([0,175,175])
    upperthresh = np.array([20,255,255])

    val=10

    c=j*5
    print(c)
    w=5

    mask1 = cv.inRange(I_HSV, np.array([c-w,175,175]), np.array([c+w,255,255]))
    mask2 = cv.inRange(I_HSV, np.array([180+c-w,175,175]), np.array([180+c+w,255,255]))

    mask=np.uint8(np.logical_or(mask1, mask2))

    res = cv.bitwise_and(Im,Im, mask= mask)

    cv.imshow('detected circles',res)
    cv.waitKey(0)

import numpy as np
import cv2 as cv
import smbus
import time

# Load up engine :)
# I2C-Adresse des MCP23017
address = 0x40
mcp23017 = smbus.SMBus(1)
# Konfiguration des MCP23017
mcp23017.write_byte_data(address,0x00,0x01) # Bank A Output
mcp23017.write_byte_data(address,0x30, 0x00) # Bank B Inputs
mcp23017.write_byte_data(address,0x31, 0x10) # Bank B Inputs

mcp23017.write_byte_data(address,0x33, 0x00) # Bank B Inputs
mcp23017.write_byte_data(address,0x32, 0x00) # Bank B Inputs

# mcp23017.write_byte_data(address,0xFE, 0x0F) # Bank B Inputs

mcp23017.write_byte_data(address,0x27, 0x10) # Bank B Inputs
mcp23017.write_byte_data(address,0x29, 0x00) # Bank B Inputs
mcp23017.write_byte_data(address,0x2B, 0x10) # Bank B Inputs
mcp23017.write_byte_data(address,0x2D, 0x00) # Bank B Inputs


# maximale Stellgroessen
an1 = 300 
an2 = -300
maxv = 2000

# Referenzabstand (Pixelbreite des Objektes)
size = 60
cent = 320
# Controlgains
kvel = 70
kangle = 20



cam = cv.VideoCapture(0)#Webcam definieren


#cam.set(cv.CAP_PROP_FRAME_WIDTH,300)
#cam.set(cv.CAP_PROP_FRAME_HEIGHT,300)
print(cam.get(cv.CAP_PROP_FRAME_WIDTH))
print(cam.get(cv.CAP_PROP_FRAME_HEIGHT))

for j in range(0,300):
    #cv.waitKey(0)#Warten
    ret, img = cam.read()#von Camera lesen

    #Gray scale, thresholding, labeling
    I = cv.cvtColor(img, cv.COLOR_RGB2HSV)#Schwarz weiss konvertierung
    ret, HI=cv.threshold(I[:,:,0],0,50,cv.THRESH_BINARY)#Thresholding
    ret, SI=cv.threshold(I[:,:,1],160,255,cv.THRESH_BINARY)#Thresholding
    ret, VI=cv.threshold(I[:,:,2],160,255,cv.THRESH_BINARY)#Thresholding
    tg_img=np.uint8(np.logical_and(HI,np.logical_and(SI,VI)))
    ret, l_img=cv.connectedComponents(tg_img,4,cv.CV_32S)#labeling

    # cv.imshow('image',HI)#Bild ausgeben
    # cv.waitKey()#Warten

    # cv.imshow('image',SI)#Bild ausgeben
    # cv.waitKey()#Warten

    # cv.imshow('image',VI)#Bild ausgeben
    # cv.waitKey()#Warten

    labels=np.amax(l_img)#Anzal der verschiedenen labels
    numlabel=np.zeros((1,labels))#Allocation

    #TODO fix bug, if numlabel =[]

    for i in range(1,labels+1):
        numlabel[0,i-1]=np.count_nonzero(l_img==i)#Anzahl der Pixel pro label

    if labels>0:
        maxlabel=np.argmax(numlabel)+1#Label mit den meisten Pixeln
    else:
        maxlabel=0

    I = 255*(l_img == maxlabel)#Auswahl der groessten Region
    I=np.uint8(I)# Konvertierung
    Rect=cv.boundingRect(I)#Finde Rechteck

    I=cv.cvtColor(I, cv.COLOR_GRAY2RGB)#Konvertierung zu RGB

    cv.rectangle(img, (Rect[0],Rect[1]),  (Rect[0]+Rect[2],Rect[1] + Rect[3]), [0, 0, 255], thickness=1, lineType=8, shift=0)#Boundingbox einzeichnen

    print((Rect[2],Rect[0]+0.5*Rect[2]))
    #cv.imwrite('im.jpg',img)
    rmess = Rect[2] 
    dmess = Rect[0]+0.5*Rect[2]
    errd = float(size) - float(rmess)
    errd = max(0, float(errd))
    ctr1 = float(errd) * float(kvel)
    ctr2 = min(int(ctr1),int(maxv))
    mcp23017.write_byte_data(address,0x34, np.int16(ctr2)) # Bank B Inputs
    mcp23017.write_byte_data(address,0x35, ((np.int16(ctr2) << 3) >> 11)) # Bank B Inputs# Close out when done 
    #cv.imshow('image',I)#Bild ausgeben
    erran = -(float(cent) - float(dmess))
    ctran = float(erran) * float(kangle)
    ctran1 = max(float(an2),float(ctran))
    ctran2 = min(float(ctran1),float(an1))
    print str(ctran2)
    servin = float(ctran2) + float('1228')
    mcp23017.write_byte_data(address,0x20, np.int16(servin)) # Bank B Inputs
    mcp23017.write_byte_data(address,0x21, ((np.int16(servin) << 3) >> 11))
	
	
mcp23017.write_byte_data(address,0x20, np.int16(1228)) # Bank B Inputs
mcp23017.write_byte_data(address,0x21, ((np.int16(1228) << 3) >> 11))
mcp23017.write_byte_data(address,0x34, np.int16(0)) # Bank B Inputs
mcp23017.write_byte_data(address,0x35, ((np.int16(0) << 3) >> 11)) # Bank B Inputs# Close out when done
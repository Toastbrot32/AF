import numpy as np
import cv2 as cv
import math

cam = cv.VideoCapture(0)#Webcam definieren

def pix2welt(pxx,pxy):
    pi=3.141
    h_Kamera=0.1
    h_Auto=0.02
    h_Ges=h_Kamera+h_Auto
    y=0.15
    alp=80*pi/180
    roh_h=0.00007
    roh_w=0.00007
    num_P_h=480
    num_P_w=640
    f=0.035

    Px=pxx-0.5*num_P_w
    Py=pxy-0.5*num_P_h
    
    Y_Kam=Py*roh_h
    alp_int=math.atan(Y_Kam/f)
    alp_Ges=alp_int + alp

    Y=math.tan(alp_Ges) * h_Ges

    A=(Y**2 + h_Ges**2)**0.5

    X_Kam=Px*roh_h
    beta_Ges=math.atan(X_Kam/f)

    X=math.tan(beta_Ges)*A

    Y_Auto=Y-y
    X_Auto=X

    Z_Auto=h_Auto

    return(Y_Auto, X_Auto, Z_Auto)

for j in range(0,1):
    
    img = cv.imread('im.jpg')#von Camera lesen

    #Gray scale, thresholding, labeling
    I = cv.cvtColor(img, cv.COLOR_RGB2HSV)#Schwarz weiss konvertierung
    ret, HI=cv.threshold(I[:,:,0],0,302,cv.THRESH_BINARY)#Thresholding
    ret, SI=cv.threshold(I[:,:,1],170,255,cv.THRESH_BINARY)#Thresholding
    ret, VI=cv.threshold(I[:,:,2],170,255,cv.THRESH_BINARY)#Thresholding
    tg_img=np.uint8(np.logical_and(HI,np.logical_and(SI,VI)))
    ret, l_img=cv.connectedComponents(tg_img,4,cv.CV_32S)#labeling
    print(np.amax(I[:,:,0]))
    #HI=cv.imread('HI.jpg')
    #HI2=1*(HI > 0)

    cv.imwrite('HI2.jpg',HI)

    labels=np.amax(l_img)#Anzal der verschiedenen labels
    numlabel=np.zeros((1,labels))#Allocation

    for i in range(1,labels+1):
        numlabel[0,i-1]=np.count_nonzero(l_img==i)#Anzahl der Pixel pro label

    if labels>0:
        maxlabel=np.argmax(numlabel)+1#Label mit den meisten Pixeln
    else:
        maxlabel=0

    I = 255*(l_img == maxlabel)#Auswahl der groessten Region
    I=np.uint8(I)# Konvertierung
    Rect=cv.boundingRect(I)#Finde Rechteck

    cv.imwrite('ML.jpg',I)

    I=cv.cvtColor(I, cv.COLOR_GRAY2RGB)#Konvertierung zu RGB

    cv.rectangle(I, (Rect[0],Rect[1]),  (Rect[0]+Rect[2],Rect[1] + Rect[3]), [0, 0, 255], thickness=1, lineType=8, shift=0)#Boundingbox einzeichnen

    cv.imwrite('ML.jpg',I)

    #cv.imshow('image',I)#Bild ausgeben
    #cv.waitKey()
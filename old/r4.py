import numpy as np
import cv2 as cv
import time

cam = cv.VideoCapture(0)#Webcam definieren


for j in range(0,1):
    ret, img = cam.read()#von Camera lesen

    I = cv.cvtColor(img, cv.COLOR_RGB2HSV)#Schwarz weiss konvertierung
    ret, HI=cv.threshold(I[:,:,0],0,50,cv.THRESH_BINARY)#Thresholding
    ret, SI=cv.threshold(I[:,:,1],160,255,cv.THRESH_BINARY)#Thresholding
    ret, VI=cv.threshold(I[:,:,2],160,255,cv.THRESH_BINARY)#Thresholding
    src2=np.uint8(np.logical_and(HI,np.logical_and(SI,VI)))
    cv.imshow('out', 255*src2)
    cv.imwrite('out.jpg',255*src2)
    cv.waitKey(1)
    time.sleep(0.3)
    
    src=255*src2
    gray = cv.medianBlur(src, 5)
    
    rows = gray.shape[0]
    circles = cv.HoughCircles(gray, cv.HOUGH_GRADIENT, 1, rows / 8,param1=100, param2=30,minRadius=1, maxRadius=30)
    
    print(circles)

    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            center = (i[0], i[1])
            # circle center
            cv.circle(src, center, 1, (0, 100, 100), 3)
            # circle outline
            radius = i[2]
            cv.circle(src, center, radius, (255, 0, 255), 3)

    cv.imshow('out', 255*src)
    cv.waitKey(1)
    time.sleep(0.2)
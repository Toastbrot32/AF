from multiprocessing import Process, Queue
from terminaltables import AsciiTable
from ampel import circ_in_img
from ampel import pstatus
from ampel import remstat
import time
import numpy as np
import cv2 as cv
import math

def f(q,cam):
    while True:
        ret, StartIm = cam.read()
        debug=1
        StatusR=circ_in_img(StartIm,60,[0,10],debug)
        StatusO=circ_in_img(StartIm,60,[20,5],debug)
        StatusG=circ_in_img(StartIm,60,[55,20],debug)
        q.put([StatusR,StatusO,StatusG])
        time.sleep(0)

def f2(q2):
    while True:
        q2.put('---')
        time.sleep(3)

if __name__ == '__main__':

    cam = cv.VideoCapture(0)#Webcam definieren

    q = Queue()
    p = Process(target=f, args=(q,cam))
    p.start()
    q2 = Queue()
    p2 = Process(target=f2, args=(q2,))
    p2.start()
    Ampel_Status=[0,0,0]


    while True:
        data=[]
        data.append(['STATUS','OK'])

        if not q.empty():
            Ampel_Status=q.get()
        if not q2.empty():
            v=q2.get()
            print(v)
        data.append(['Rot',str(Ampel_Status[0])])
        data.append(['Orange',str(Ampel_Status[1])])
        data.append(['Gruen',str(Ampel_Status[2])])
        pstatus(data)
        #print(Ampel_Status)
        time.sleep(0.001)
        remstat(data)

    
    p.join()
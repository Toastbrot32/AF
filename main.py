from multiprocessing import Process, Queue
#from terminaltables import AsciiTable
from ampel import circ_in_img
from ampel import pstatus
from ampel import remstat
from hough import street
from hough import frame_to_lines
from kreuzung import bei_kreuzung
import numpy as np
import cv2 as cv
import math
import time
from hough import frame_to_lines

def ampel_check(q,q2):
    while True:
        if not q2.empty():
            StartIm=q2.get()
            #print('----amp')
            debug=1
            StatusR=circ_in_img(StartIm,60,[0,10],debug)
            StatusO=circ_in_img(StartIm,60,[20,5],debug)
            StatusG=circ_in_img(StartIm,60,[55,20],debug)
            q.put([StatusR,StatusO,StatusG])
            time.sleep(0.001)

def str_check(q,q2):
    while True:
        if not q2.empty():
            frame=q2.get()
            #print('----str')
            frame=frame[150:400,:]
            h_sep=50

            frame2=frame[h_sep:300,:]

            p1, a1 , a= frame_to_lines(frame2,45,0.75,50,200)
            q.put([p1,a1,a])
            #print(p1)
            time.sleep(0.001)

def linie_check(q,q2):
    while True:
        if not q2.empty():
            frame=q2.get()
            res = bei_kreuzung(frame)
            q.put(res)
            time.sleep(0.001)


if __name__ == '__main__':

    #cam = cv.VideoCapture(0)#Webcam definieren
    cam2 = cv.VideoCapture("Halle3.avi")
    ret, frame = cam2.read()

    #Ampel Prozess
    ampel_queue = Queue()
    ampel_in_queue = Queue()
    p = Process(target=ampel_check, args=(ampel_queue,ampel_in_queue))
    p.start()

    #Strassen Prozess
    str_queue = Queue()
    str_in_queue = Queue()
    p2 = Process(target=str_check, args=(str_queue,str_in_queue))
    p2.start()

    #Linien Prozess
    linie_queue = Queue()
    linie_in_queue = Queue()
    p3 = Process(target=linie_check, args=(linie_queue,linie_in_queue))
    p3.start()

    #Initialisierung
    Ampel_Status=[0,0,0]
    Str_Status=[0,0,0]
    Linie_Status=0
    phase=0
    ampel_in_queue.put(frame)
    str_in_queue.put(frame)
    linie_in_queue.put(frame)

    #Hauptschleife
    while True:
        #Initialisierung
        data=[]
        data.append(['STATUS','OK'])


        #Bild auslesen
        ret, frame = cam2.read()

        if not ampel_queue.empty():
            #Ergbnisse vom Ampelprozess holen
            Ampel_Status=ampel_queue.get()
            #Neues Bild an Ampelprozess senden
            ampel_in_queue.put(frame)

        if not str_queue.empty():
            #Ergbnisse vom Strassenprozess holen
            Str_Status=str_queue.get()
            #Neues Bild an Strassenprozess senden
            str_in_queue.put(frame)

        if not linie_queue.empty():
            #Ergbnisse vom Strassenprozess holen
            Linie_Status=linie_queue.get()
            #Neues Bild an Strassenprozess senden
            linie_in_queue.put(frame)


        if Linie_Status == 1:
            phase=1
        else:
            phase = 0


        if phase == 0:
            #Regelung für die Strasse
            print('--')

        if phase == 1:
            if Ampel_Status[2] == 1:
                print('---')
                phase == 2

            if Ampel_Status[0] == 1 or Ampel_Status[1] == 1:
                print('---')
                #Regelung, stehenbleiben

        if phase == 2:
            print('---')
            #Hier Maneuver
            
            

        data.append(['Rot',str(Ampel_Status[0])])
        data.append(['Orange',str(Ampel_Status[1])])
        data.append(['Gruen',str(Ampel_Status[2])])
        data.append(['x',str(Str_Status[0])])
        data.append(['theta',str(Str_Status[1])])
        data.append(['Winkel',str(Str_Status[2])])
        data.append(['Linie',str(Linie_Status)])
        data.append(['Phase',str(phase)])

        pstatus(data)
        #print(Ampel_Status)
        time.sleep(0.1)
        #print('---')
        remstat(data)

    
    p.join()
